import React from 'react';
import {createStackNavigator} from '@react-navigation/stack'
import {NavigationContainer} from '@react-navigation/native'
import Login from '../components/login'
import SignUp from '../components/signup'

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen 
            name="Login" 
            component={Login} 
            options={{
                title: 'Present Value',
                headerStyle:{
                    backgroundColor: '#005da8',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                }
            
            }}
            />
        <Stack.Screen 
            name="Signup" 
            component={SignUp} 
            options={{
                title: 'Free Sign up',
                headerStyle:{
                    backgroundColor: '#005da8',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                }
            }}
            />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;