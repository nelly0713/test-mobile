import { StyleSheet } from 'react-native';


export const styles = StyleSheet.create({

  //Login Style
    container: {
      flex: 1,
      backgroundColor: '#fff',
      // alignItems: 'center',
      // justifyContent: 'center',
    },
    loginBox: {
      //alignItems: 'center',
      //justifyContent: 'center',
      margin: 30,
      flex: 1,
      alignItems: 'center',
    },
    GreatingsTop: {
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      color: '#005da8',
    },
    Greatingsbottom: {
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 15,
      color: '#ddd',
      marginBottom: 20,
    },
    GreatingsLine: {
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 30,
      paddingBottom: 20,
      color: '#005da8',
    },
    forgotpass: {
      fontSize: 15,
      color: '#19ca8f',
      marginTop: 10,
    },
    SignIn: {
      color: '#19ca8f',
      fontWeight: 'bold',
      padding: 10,
    },
    SignInBox :{
      borderColor: '#19ca8f',
      borderWidth: 2,
      borderRadius: 20,
      width: '50%',
      alignItems: 'center',
      marginTop: 30,
  
    },
    Inputfeld: {
      width: 250,
      paddingLeft: 10,
      
    },
    InputDesign: {
      alignItems: 'center',
      flexDirection: 'row',
      borderRadius: 5,
      backgroundColor: '#f1f5f9',
      width: 300,
      marginTop: 10,
      height: 50,
      paddingLeft: 20,
    },
  ///End of logIn Style

 ////Sign Up Style   
    HeaderBg: {
      //paddingTop: 20,
     // height: 80,
      flex: 1,
      backgroundColor: '#fff',
  },
  inner:{
    flex: 1,
    justifyContent: "flex-end",
  },
  Headertitle: {
      paddingTop: 25,
      fontWeight: 'bold',
      fontSize: 25,
      color: '#005da8',
      textAlign: 'center',
      lineHeight: 5,
  },
  infode:{
      textAlign: 'center',
      padding: 15,
      fontSize: 15,
      color: '#777',
  },
  nameComp:{
      justifyContent: 'center',

  },
  nameComp1:{
      borderRadius: 5,
      //borderColor: '#ddd',
      //borderWidth: 1,
      height: 35,
      marginStart: 20,
      marginEnd: 20,
      marginBottom: 10,
      backgroundColor: '#f1f5f9',
      paddingStart: 10,
      justifyContent: 'center',
  },
  Comment:{
      borderRadius: 5,
     //borderColor: '#ddd',
      //borderWidth: 1,
      height:70,
      marginStart: 20,
      marginEnd: 20,
      marginBottom: 10,
      backgroundColor: '#f1f5f9',
      paddingStart: 10,
  },
  policy:{
      flexDirection: 'row',
      marginStart: 20,
      marginEnd: 20,
      marginBottom: 10,
      paddingEnd: 30,
  },
  SignUpBottom: {
    alignItems: 'center',
  },
  signupBox:{
    borderColor: '#19ca8f',
      borderWidth: 2,
      borderRadius: 20,
      width: '50%',
      alignItems: 'center',
      marginTop: 15,
  }
  
  });
  