import React, {useState} from 'react';
import { Text, View, TextInput, TouchableWithoutFeedback, Keyboard, CheckBox,TouchableOpacity, KeyboardAvoidingView, SafeAreaView } from 'react-native';
import {styles} from '../style/globalstyle';


export default function Signup({navigation}){

    const [isSelected, setSelection] = useState(false);

    return(

       <KeyboardAvoidingView
        style={{flex: 1}} 
        behavior="padding">

            <SafeAreaView style={styles.HeaderBg}>

                <TouchableWithoutFeedback onPress={ () => {
                    Keyboard.dismiss();
                }}>
                    <View style={styles.inner}>
                        <View>
                            <Text style={styles.Headertitle}>
                                Sign up for a free trial now.
                            </Text>
                            <Text style={styles.Headertitle}>
                                _____
                            </Text>
                            <Text style={styles.infode}>
                                Fill out the form below to request a free trial. Fields with * are required.
                            </Text>
                        </View>
                        <View style={styles.nameComp}>
                            <View style={styles.nameComp1}>
                                <TextInput 
                                    placeholder='Name*'
                                />
                            </View>
                            <View style={styles.nameComp1}>
                                <TextInput 
                                    placeholder='Company*'
                                />
                            </View>
                            <View style={styles.nameComp1}>
                                <TextInput 
                                    placeholder='Phone*'
                                />
                            </View>
                            <View style={styles.nameComp1}>
                                <TextInput
                                    placeholder='Email*'
                                />
                            </View>
                            <View style={styles.Comment}>
                                <TextInput 
                                    editable
                                    numberOfLines={4}
                                    maxLength={40}
                                    multiline
                                    placeholder='Comment'
                                />
                            </View>
                        </View>
                        <View style={styles.policy}>
                            <CheckBox
                                value ={isSelected}
                                onValueChange={setSelection}
                            />
                            <Text>
                                I declare my consent to the use of my e-mail address, my name, my phone number and the message for answering my contact request as outlined in the privacy policy.
                                {isSelected ? true : false}
                            </Text>
                        </View>
                        <View style={styles.SignUpBottom}>
                            <View style={styles.signupBox}>
                                <TouchableOpacity >
                                <Text style={styles.SignIn}>SIGN UP</Text>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => navigation.goBack()}>
                                <Text style={styles.SignIn}>SIGN IN INSTEAD</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>

                </TouchableWithoutFeedback>

            </SafeAreaView>

        </KeyboardAvoidingView>

    )
}
