import React from 'react';
import { Text, View, TextInput, TouchableOpacity, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { FontAwesome } from '@expo/vector-icons'; 
import {styles} from '../style/globalstyle';

export default function LoginPage({navigation}) {
  return (
    <TouchableWithoutFeedback onPress={ () => {
        Keyboard.dismiss();
      }}>
      <View style={styles.container}>
        <View style={styles.loginBox}>
          <View>
            <Text style={styles.GreatingsTop}>It's all happening here.</Text>
            <Text style={styles.GreatingsLine}>_____</Text>
            <Text style={styles.Greatingsbottom}>We are pleased to welcome you!</Text>
          </View>
          <View>
            <View style={styles.InputDesign}>
              <FontAwesome name="question-circle" size={25} color="grey" />
              <TextInput style={styles.Inputfeld}
              placeholder=' User ID or Email'
              />
            </View>
            <View style={styles.InputDesign}>
              <FontAwesome name="eye-slash" size={25} color="grey"  />
              <TextInput style={styles.Inputfeld}
              placeholder=' Password'
              />
            </View>
          </View>
          <View>
              <TouchableOpacity>
                <Text style={styles.forgotpass}>Forgot your password?</Text>
              </TouchableOpacity>
            </View>
          <View style={styles.SignInBox}>
            <TouchableOpacity >
              <Text style={styles.SignIn}>SIGN IN</Text>
            </TouchableOpacity>
          </View>
          <View>
            <TouchableOpacity onPress={() => navigation.navigate('Signup')}>
              <Text style={styles.SignIn}>SIGN UP FOR FREE TRIAL</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}